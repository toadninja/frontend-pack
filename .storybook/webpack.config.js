const path = require("path");
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    module: {
        rules: [
            // {
            //     test: /\.scss$/,
            //     loaders: ["style-loader", "css-loader", "sass-loader"],
            //     include: path.resolve(__dirname, "../")
            // }
            {
                test : /\.styl(us)?$/,
                use  : [
                    'vue-style-loader',
                    'css-loader',
                    'stylus-loader',
                ],
            },
            {
                test : /\.css$/,
                use  : [
                    'vue-style-loader',
                    'css-loader',
                ],
            },
            // {
            //     test    : /\.js$/,
            //     loader  : 'babel-loader',
            //     exclude : /node_modules/,
            //     options : {
            //         presets: [ "@babel/preset-env", ] //Preset used for env setup
            //     },
            // },
        ]
    }
};
